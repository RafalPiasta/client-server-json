import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private static String command;

    public void startClient(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
    }

    public void sendServer() throws IOException {
        this.out = new PrintWriter(clientSocket.getOutputStream());
        Gson gson = new Gson();
        Scanner scanner = new Scanner(System.in);
        command = scanner.nextLine();
        String jsonString = gson.toJson(command);
        out.println(jsonString);
        out.flush();
        }

    public void recClient() throws IOException {
        InputStreamReader in = new InputStreamReader(clientSocket.getInputStream());
        BufferedReader bf = new BufferedReader(in);
        Gson gson = new Gson();
        String json = bf.readLine();
        String str = gson.fromJson(json, String.class);
        System.out.println(str);
    }

    public void stopClient() throws IOException {
        clientSocket.close();
        out.close();
    }

    public static void main(String[] args) throws IOException {
        Client c1 = new Client();
        c1.startClient("127.0.0.1", 6666);
        System.out.println("Lista komend:");
        System.out.println("uptime");
        System.out.println("info");
        System.out.println("help");
        System.out.println("stop");
        while (true) {
            c1.sendServer();
            c1.recClient();
            if (command.equals("stop")) {
                c1.stopClient();
                break;
            }
        }
    }
}