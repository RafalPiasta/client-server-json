import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Server {
    private Socket clientSocket;
    private static String str;
    private final String serverVersion = "v1.0";
    private LocalDate startDate = LocalDate.now();
    public LocalTime startTime = LocalTime.now();

    public void startServer(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();
    }

    public void sendServer(String reply) throws IOException {
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream());
        Gson gson = new Gson();
        String jsonString = gson.toJson(reply);
        out.println(jsonString);
        out.flush();
    }

    public void recClient() throws IOException {
        InputStreamReader in = new InputStreamReader(clientSocket.getInputStream());
        BufferedReader bf = new BufferedReader(in);
        Gson gson = new Gson();
        String json = bf.readLine();
        str = gson.fromJson(json, String.class);
    }

    public void help() throws IOException {
        sendServer("""
                uptime - czas działania serwera,
                info   - wersja serwera oraz data jego utworzenia,
                help   - lista dostępnych poleceń,
                stop   - zatrzymuje działanie serwera.""");
    }

    public void stop() throws IOException {
        sendServer("Serwer wyłączony.");
        clientSocket.close();
    }

    public void uptime() throws IOException {
        long durationSec = startTime.until(LocalTime.now(), ChronoUnit.SECONDS);
        long days = durationSec / 86_400;
        long hours = durationSec / 3_600 - days * 24;
        long minutes = durationSec / 60 - days * 1_440 - hours * 60;
        long seconds = durationSec - days * 86_400 - hours * 3_600 - minutes * 60;
        String duration = days + "d:" + hours + "h:" + minutes + "m:" + seconds + "s";
        sendServer(duration);
    }

    public void info() throws IOException {
        sendServer("Wersja serwera: " + serverVersion + "\n" + "Data włączenia serwera: " + startDate);
    }

    public static void main(String[] args) throws IOException {
        Server s1 = new Server();
        s1.startServer(6666);
        while (true) {
            s1.recClient();
            if (str.equals("help")) {
                s1.help();
            } else if (str.equals("uptime")) {
                s1.uptime();
            } else if (str.equals("info")) {
                s1.info();
            } else if (str.equals("stop")) {
                s1.stop();
                break;
            } else {
                s1.sendServer("Podaj poprawne polecenie. Wpisz \"help\" aby dowiedzieć się więcej.");
            }
        }



    }
}